---
title: "Creating reproducible documents with R markdown"
author: "Sara Tomiolo"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	echo = FALSE,
	message = FALSE,
	warning = FALSE
)

library(tidyverse)

```
## Session details and objectives

1. Become aware of the importance of reproducibility in science.
2. Become aware of the efficiency of being reproducible.
3. Learn the basics of creating reproducible documents in R containing code, graphs, images, links, tables (and references)


## What is reproducibility?

1. Difference between replicability and reproducibility

**Reproducibility**: Generating the same exact results when using the same data and code.

**Replicability**: Repeating a study by performing independently an identical study.

A recent [survey](https://www.nature.com/news/polopoly_fs/1.19970!/menu/main/topColumns/topLeftColumn/pdf/533452a.pdf) published in the journal Nature indicated that 70% of researchers failed to reproduce another scientist's experiment and more than 50% was not able to reproduce their own results. The potential reasons of why so many studies are not reproducible are described in the survey. 

However, the growing awareness of the reproducibility crisis and its potential implications on the quality and reliability of the science that we publish and read, motivated measures to increase the reproducibility of published research, for example by means of publishing data and statistical code. 


### Why should we care about being reproducible?

Besides the obvious desire to produce scientific results that are reliable and can further the knowledge and progress in our field of research, being reproducible can help us work better and more efficiently.

![Source [data sci quotes](https://github.com/kbroman/datasciquotes)](your_worst_collaborator.png)


Often enough we forget how important it is to keep our R scripts clear and understandable,
not just to ourselves in the moment, but also to collaborators and peers who may want to repeat our analyses in the future.

What appears logic and clearly documented right now may become a messy blur of uncanny code in the round of months or even weeks.

To this end it is vital that we build a **routine of accountability**, where we save some extra time everyday to go over our code and make sure it makes sense now and still will make sense if we come back to it in a few weeks. 

### How can we make sure that our research is reproducible?

- Be aware of tools available,

- Adopt a consistent workflow for doing analyses,

- Integrate the different steps of the scientific process, such as data wrangling, data analysis, visualization of results and report writing in a clear, well documented and dynamic way.

A powerful tool that can help us is **R Markdown**.

## What is R Markdown?

A [markup syntax] that allows you to write a plain text (e.g. R code and scripts) document that can convert to a range of document formats, such as Word, PDF, html.

For example, this website was created using R Markdown.

In R and R Markdown, the software that allows the conversion from R code into different document formats is called [pandoc].  

In R Markdown it is possible to write text and integrate it with code. In R scripts you would need to use `#`so that your comments are not read as code. 

Many of us would normally carry out different steps of their study/investigation in separate phases and programs (analyses and graphs in R, text in your word processor) and then integrate it through a work of copy-and-paste.

With R Markdown it is possible to carry out this work in the same environment and update the document instantly and dynamically.

[markup syntax]: https://en.wikipedia.org/wiki/Markup_language
[pandoc]: http://pandoc.org/index.html


## Syntax in R Markdown

- There are some norms that allow for elements in Markdown to be converted in different text formats such as headers, bold, italics.


### Headers
Are used to create chapters and sections

```markdown
## Header 1

Paragraph

### Header 2

Paragraph

#### Header 3

Paragraph

```
...would look like


## Header 1

Paragraph

### Header 2

Paragraph

#### Header 3

Paragraph


### Text formatting

`**bold**` gives **bold**

`*italics*` gives *italics*

`super^script^` gives super^script^

`sub~script~` gives sub~script~

### Lists
 
**Unnumbered**
 
```markdown
- Item 1
- Item 2
- Item 3

```
- Item 1
- Item 2
- Item 3

**Numbered**

```markdown
1. Item 1
2. Item 2
3. Item 3

```
1. Item 1
2. Item 2
3. Item 3


### Block quotes

```markdown
> Say something smart in here...

```

> Say something smart in here...


### Footnotes

```markdown
Footnote[^1]

[^1]: Footnote content
```

Footnote[^1]

[^1]: Footnote content


### Insert images

```markdown
![image caption](reproducibile_research_data_analysis.png)
```
![image caption](reproducible_research_data_analysis.png)


### Adding links

You can add links to your text 

```markdown
[Link](https://international.au.dk/)
```
[Link](https://international.au.dk/)


### YAML header

The [YAML] header is a string of metadata located at the top of your R Markdown document and delimited by `---`.
It contains information about the author, the title and the date in which the document was created, but also information on the output of the document.

```yaml
----
title: "Your document"
author: Your Name
output: word_document

----
```



There are several options for the output of your document. HTML, Word and PDF are some examples. 

Markdown was originally designed for html outputs, and indeed this format offers the widest array of features.

If you want to output your document in PDF format you will have to install the package [tinytex]. 


If you want to output your document in Word with a pre-set formatting, for example when you are writing a manuscript draft or a report with a conventional format, you will have to create a reference style document in word and link it in the YAML reader.

We don't have time to go through this today, but you can find a very detailed tutorial in [here](https://bookdown.org/yihui/rmarkdown/html-document.html)


[YAML]:https://bookdown.org/yihui/rmarkdown/html-document.html

[tinytex]: https://yihui.name/tinytex/



### Inserting code chunks

One of the most prominent features of R Markdown is that you can insert and run code chunks from within the document.

This represents a very powerful tool. 

R Markdown can be used for annotating your analyses and scripts in a way that is less heavy and 
more agile than R script, where annotations would all be marked with `#`. 

Similarly, R Markdown can be used to write manuscripts and reports and integrating easily and dynamically graphs and analyses.  

Normally, when writing your manuscript in Word or any other word-processing software, you would re-run your analyses in R, then copy and paste your new results into your manuscript.

This is generally where most mistakes happen, especially after several rounds of copy-paste.

With R Markdown, you can simply modify the analysis inside the code chunks and they will be updated automatically once you output your document.

Code chunks are created by pressing `Ctrl + Alt + I`

Before you can run anything you need to tell your R Markdown document where to find functions and files that you will use.
At the top of your document, after the YAML header, you will want to add a code chunk where you set up all the files and library you will need

````markdown
`r ""````{r setup, include=FALSE}

library(tidyverse)
library(viridis)

`r ""````
````

You can add options to the code chunk to avoid code showing in your output for example `echo = FALSE` and in the case of figures you can customize the plot title and size.

```{r echo = FALSE, fig.cap = "title", fig.height = 8, fig.width = 8}
ggplot(iris, aes(x = Petal.Length, y = Petal.Width, colour = Species)) +
geom_point() +
theme_minimal() +
scale_colour_viridis_d() +
labs(x = "Length", y = "Width")
```



````markdown
`r ""````{r echo = FALSE, fig.cap = "title", fig.height = 8, fig.width = 8}
ggplot(iris, aes(x = Petal.Length, y = Petal.Width, colour = Species)) +
geom_point() +
theme_minimal() +
scale_colour_viridis_d() +
labs(x = "Length", y = "Width")

`r ""````
````

```{r echo = FALSE, fig.cap = "title", fig.height = 8, fig.width = 8}
ggplot(iris, aes(x = Petal.Length, y = Petal.Width, colour = Species)) +
geom_point() +
theme_minimal() +
scale_colour_viridis_d() +
labs(x = "Length", y = "Width")
```


#### Exercise:

- **create a document with two headers and a text paragraph that uses regular, bold and italics font**

- **Insert an image**

- **Insert a code chunk and create a simple plot using the database `iris`**

### Using R Markdown with real data

We have covered the basics of R Markdown syntax and have seen how to insert code chunks and create graphs. 

How can we weave in these new knowledge and use it for producing a reproducible dynamics report?

We are going to use a database published in 2019 as supporting material for a paper published on the [ecological drivers of interspecific variation in bird songs complexity](https://onlinelibrary.wiley.com/doi/abs/10.1111/jav.02020). 


Download the database from the data folder in the workshop website. 
From the top menu select [*Course Material* > Data for Workshop](https://saratomiolo.gitlab.io/reproducibilityworkshop/).
This will immediately prompt the download of a .csv file. 
Save the data file as `song.csv` in the *data* folder of the repository you created for this workshop. 

Once the data is in your repository you will have to read it into your R markdown document.
For this we are going to use the function `read_csv` from the package `readr` (readr is one of the packages forming the tidyverse).

If you remember, during last workshop we talked about the importance of structuring your project in separate folders such as folders for data, for R scripts and for documents and reports...

This means that your data file is in a different folder (the folder "data") than your R Markdown document (the folder "doc"). To make sure that R Studio *knows* where to look when importing data, we are going to use the `here` function from the `here` package. 

`here::here` is making sure that your data file (as long as it is properly saved in your project) can be found and imported into a document (R Markdown or R script) that is saved in a different folder

```{r, echo=TRUE}
song <- readr::read_csv(here::here("data/SongComplexityData.csv"))
```


#### Exercise

**For this exercise you will work in groups of 2-3 people.**

1. Inspect the database you just saved. Formulate two questions you want to investigate with the data at your disposal.

2. Create appropriate graphs that allows for visual inspection of the data according to the 2 questions you just formulated.

3. Write a short report indicating your questions, with appropriate syntax, and the graphs that you obtained using code chunks.


### Resources for additional learn and help

- [Introduction to Rmarkdown](https://rmarkdown.rstudio.com/lesson-1.html)

- [Basic syntax in Rmarkdown](https://rmarkdown.rstudio.com/authoring_basics.html)

- [R Markdown definitive guide](https://bookdown.org/yihui/rmarkdown/)

- [Useful R Studio cheatsheets](https://www.rstudio.com/resources/cheatsheets/)

- [Guide to Pandoc](https://pandoc.org/MANUAL.html#pandocs-markdown)


### Acknowledgments:
The course material is licensed under the [Creative Commons Attribution 4.0 International License]

[Creative Commons Attribution 4.0 International License]: https://creativecommons.org/licenses/by/4.0/

Most of the material comes from a course created by Luke W. Johnston titled: [*"Reproducible Quantitative Methods: Data analysis workflow using R"*]

The database on song complexity used for the exercise provides data on ecological drivers of interspecific variation in song complexity in birds and was published by Crouch et al. on [*Dryad*]


[*"Reproducible Quantitative Methods: Data analysis workflow using R"*]: https://rqawr-course.lwjohnst.com/

[*Dryad*]:(https://datadryad.org/stash/dataset/doi:10.5061/dryad.1mv3348)


