---
title: "Free R-course in Silkeborg"
output: 
    html_document: 
        toc: false
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	echo = FALSE,
	message = FALSE,
	warning = FALSE
)

library(tidyverse)
library(viridis)
```

 *(17 September and 1 October 2019)*
 
**Learn to create amazing graphs and make your research reproducible using R, RStudio and RMarkdown (two introductory workshops)**

Learning to use new tools that will make our workflow better and our life easier is on our wish-list, but often gets put off because of time constraints or because it feels daunting to tackle something new alone.

The aim of these workshops is to:

- “Plant the seeds” for learning new (and possibly better and faster) ways to create amazing graphs using ggplot and produce reproducible reports using RMarkdown;

- Meet other people interested in improving their R skills and help each other in the process of learning.

The workshops are hands-on coding sessions targeted to people using (or planning to use) R and Rstudio. 
While it is not necessary to be an advanced R user, it would be beneficial to have some familiarity with R. 

**Plus there will be cake!**

*Teacher*: Sara Tomiolo


