

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

The course material is licensed under the
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

Most of the material comes from a course created by Luke W. Johnston titled:
[*"Reproducible Quantitative Methods: Data analysis workflow using R"*](https://rqawr-course.lwjohnst.com/).

